get access URLs:
2011 BHADRON : dirac-dms-lfn-accessURL --Protocol=xroot,root /lhcb/LHCb/Collision11/BHADRON.MDST/00041838/0000/00041838_00000383_1.bhadron.mdst
root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision11/BHADRON.MDST/00041838/0000/00041838_00000383_1.bhadron.mdst

2011 BHADRONCOMPLETEEVENT : dirac-dms-lfn-accessURL --Protocol=xroot,root /lhcb/LHCb/Collision11/BHADRONCOMPLETEEVENT.DST/00041838/0000/00041838_00000739_1.bhadroncompleteevent.dst
root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision11/BHADRONCOMPLETEEVENT.DST/00041838/0000/00041838_00000739_1.bhadroncompleteevent.dst

2012 BHADRON : dirac-dms-lfn-accessURL --Protocol=xroot,root /lhcb/LHCb/Collision12/BHADRON.MDST/00041834/0000/00041834_00000057_1.bhadron.mdst
root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/BHADRON.MDST/00041834/0000/00041834_00000057_1.bhadron.mdst

2012 BHADRONCOMPLETEEVENT : dirac-dms-lfn-accessURL --Protocol=xroot,root /lhcb/LHCb/Collision12/BHADRONCOMPLETEEVENT.DST/00041834/0000/00041834_00000895_1.bhadroncompleteevent.dst
root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/BHADRONCOMPLETEEVENT.DST/00041834/0000/00041834_00000895_1.bhadroncompleteevent.dst

2015 BHADRON : dirac-dms-lfn-accessURL --Protocol=xroot,root  /lhcb/LHCb/Collision15/BHADRON.MDST/00049671/0000/00049671_00000193_1.bhadron.mdst
root://f01-080-125-e.gridka.de:1094/pnfs/gridka.de/lhcb/LHCb/Collision15/BHADRON.MDST/00049671/0000/00049671_00000193_1.bhadron.mdst

2015 BHADRONCOMPLETEEVENT : dirac-dms-lfn-accessURL --Protocol=xroot,root  /lhcb/LHCb/Collision15/BHADRONCOMPLETEEVENT.DST/00049671/0000/00049671_00000663_1.bhadroncompleteevent.dst
root://ccdcacli067.in2p3.fr:1094/pnfs/in2p3.fr/data/lhcb/LHCb/Collision15/BHADRONCOMPLETEEVENT.DST/00049671/0000/00049671_00000663_1.bhadroncompleteevent.dst

2016 BHADRON : dirac-dms-lfn-accessURL --Protocol=xroot,root /lhcb/LHCb/Collision16/BHADRON.MDST/00052200/0000/00052200_00000258_1.bhadron.mdst
root://ccdcacli067.in2p3.fr:1094/pnfs/in2p3.fr/data/lhcb/LHCb/Collision16/BHADRON.MDST/00052200/0000/00052200_00000258_1.bhadron.mdst

2016 BHADRONCOMPLETEEVENT : dirac-dms-lfn-accessURL --Protocol=xroot,root /lhcb/LHCb/Collision16/BHADRONCOMPLETEEVENT.DST/00052200/0000/00052200_00000030_1.bhadroncompleteevent.dst
root://clhcbdlf.ads.rl.ac.uk//castor/ads.rl.ac.uk/prod/lhcb/LHCb/Collision16/BHADRONCOMPLETEEVENT.DST/00052200/0000/00052200_00000030_1.bhadroncompleteevent.dst?svcClass=lhcbDst
