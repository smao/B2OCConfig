"""
Stripping filtering file for B->D(Kshh)h Monte Carlo
@author Chris Thomas
@date   2012-11-20
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object
#

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping20'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=config, streamName=streamName, archive=archive)

streams = []

###########################################
###########################################

_filterlines = quickBuild('Bhadron')

# Select lines you want
# Stream name will control name in book-keeping - make it something descriptive
MyStream = StrippingStream("B2DHKsHHGam.Strip")
# Select lines by name
MyLines = [ 'StrippingB2D0KD2KSHHLLBeauty2CharmLine', 'StrippingB2D0PiD2KSHHLLBeauty2CharmLine', 'StrippingB2D0KD2KSHHDDBeauty2CharmLine',
            'StrippingB2D0PiD2KSHHDDBeauty2CharmLine', 'StrippingB2D0KD2KSHHLLWSBeauty2CharmLine', 'StrippingB2D0PiD2KSHHLLWSBeauty2CharmLine',
            'StrippingB2D0KD2KSHHDDWSBeauty2CharmLine', 'StrippingB2D0PiD2KSHHDDWSBeauty2CharmLine' ]

for line in _filterlines.lines :
    if line.name() in MyLines:
        MyStream.appendLines( [ line ] ) 

# Set prescales to 1.0 if necessary
for line in _filterlines.lines:
    line._prescale = 1.0
    
# Configure Stripping
from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = [ MyStream ],
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents )

MyStream.sequence().IgnoreFilterPassed = False # so that we get only selected events written out

###########################################
###########################################

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )


#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default'              : stripDSTElements()
    }


SelDSTWriterConf = {
    'default'              : stripDSTStreamConf()
    }

for stream in sc.activeStreams() :
   print "there is a stream called " + stream.name() + " active"

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='',
                          SelectionSequences = sc.activeStreams()
                          )


#----------Include trigger filtering---------------------------

# Bring in the filter
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (

    #No trigger filtering required
    #HLT_Code   = "HLT_PASS_RE('Hlt1Track.*Decision') & HLT_PASS_RE('Hlt2.*Topo.*Decision')" 

    )

#----------------------------------------------------------------

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2011"
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"

#
# Need to add bank-killer for testing
#from Configurables import EventNodeKiller
#eventNodeKiller = EventNodeKiller('Stripkiller')
#eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]
#DaVinci().appendToMainSequence( [ eventNodeKiller ] )   # Kill old stripping banks first
#

DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
#DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')
#DaVinci().UseTrigRawEvent=True
