"""
Stripping filtering file for Bd->D(Kshh)K*(Kpi) Monte Carlo
@author Alexis Vallier
@date   2013-02-15
"""
from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

# Standard stripping20
stripping='stripping20'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

################################################
################################################

stream = buildStream(stripping=config, streamName='Bhadron', archive=archive)

# Select my lines
MyStream = StrippingStream("B02DKst_D2Kshh.Strip")
MyLines = [ 'StrippingB02D0KPiD2KSHHLLBeauty2CharmLine','StrippingB02D0KPiD2KSHHDDBeauty2CharmLine' ]

for line in stream.lines:
    # Set prescales to 1.0 if necessary
    line._prescale = 1.0
    if line.name() in MyLines :
        MyStream.appendLines( [ line ] )
    
# Configure Stripping
from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = [ MyStream ],
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents )

# Set Filtered mode
MyStream.sequence().IgnoreFilterPassed = False


################################################
################################################

#
# Configuration of SelDSTWriter
#
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default'              : stripDSTElements()
    }


SelDSTWriterConf = {
    'default'              : stripDSTStreamConf()
    }

for stream in sc.activeStreams() :
   print "there is a stream called " + stream.name() + " active"

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='',
                          SelectionSequences = sc.activeStreams()
                          )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2012"
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"
#DaVinci().TupleFile = "SelB02DKst_D2Kshh.Strip.dst"

DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )

