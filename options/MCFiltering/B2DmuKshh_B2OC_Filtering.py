"""
Options for building Stripping21 on MC,
Trigger and stripping filtering for B+ -> D0(Kshh) mu nu and B0 -> D*0(D0(Kshh)pi) mu nu in B2OC stripping lines
@author Faye Cheung
@date   2015-09-03
"""
#stripping version
stripping='stripping21'

#use CommonParticlesArchive
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

# Tighten Trk Chi2 to <3
from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 3 ],
                                "CloneDistCut" : [5000, 9e+99 ] }

#
#Raw event juggler to split Other/RawEvent into Velo/RawEvent and Tracker/RawEvent
#
from Configurables import RawEventJuggler
juggler = RawEventJuggler( DataOnDemand=True, Input=2.0, Output=4.0 )

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive

#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)


#
# Merge into one stream and run in flag mode
#
AllStreams = StrippingStream("B2DmuKshhB2OC.StripTrig")
linesToAdd = []
MyLines = ['StrippingB02DstarMuNuDst2D0Pi_D2KSHHDDBeauty2CharmLine',
           'StrippingB02DstarMuNuDst2D0Pi_D2KSHHLLBeauty2CharmLine',
           'StrippingB02DstarMuNuWSDst2D0Pi_D2KSHHDDBeauty2CharmLine',
           'StrippingB02DstarMuNuWSDst2D0Pi_D2KSHHLLBeauty2CharmLine',
           'StrippingB2D0MuNuD2KSHHDDBeauty2CharmLine',
           'StrippingB2D0MuNuD2KSHHLLBeauty2CharmLine'
          ]

for stream in streams:
    if 'Bhadron' in stream.name():
        for line in stream.lines:
            line._prescale = 1.0
            if line.name() in MyLines:
                linesToAdd.append(line)
AllStreams.appendLines(linesToAdd)

sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip'
                    )

AllStreams.sequence().IgnoreFilterPassed = False # so that only events passing the lines requested are stored

#
# Configuration of SelDSTWriter
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking,selectiveRawEvent=False)
    }

#Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]

# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x36112100)

# Trigger filtering

from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    L0DU_Code = "L0_CHANNEL_RE('Muon|DiMuon') | L0_CHANNEL('Hadron')",
    HLT_Code = "(HLT_PASS_RE('Hlt1Track.*Decision') | HLT_PASS_RE('Hlt1.*Muon.*Decision')) & (HLT_PASS_RE('Hlt2.*Topo.*Decision') | HLT_PASS_RE('Hlt2.*Muon.*Decision'))" 
    )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"
DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

