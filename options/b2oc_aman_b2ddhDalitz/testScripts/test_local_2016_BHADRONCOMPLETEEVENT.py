from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
     'PFN:root://clhcbdlf.ads.rl.ac.uk//castor/ads.rl.ac.uk/prod/lhcb/LHCb/Collision16/BHADRONCOMPLETEEVENT.DST/00052200/0000/00052200_00000030_1.bhadroncompleteevent.dst?svcClass=lhcbDst' ] )

import sys
sys.path.insert(0,"/afs/cern.ch/user/j/johndan/Analysis/B2DDh/lhcb-b2oc-ddk-dalitz/DataProcessing/Data/2_WGProd/DaVinciDev_v42r6p1/WG/B2OCConfig/python")

# Helper file to define data type for davinci

the_year      = '2016'
the_fileType  = 'DST'
the_rootintes = ""
the_locationRoot = "/Event/BhadronCompleteEvent/"

from Configurables import DaVinci, CondDB
dv = DaVinci ( DataType                  = the_year           ,
               InputType                 = the_fileType       ,
               RootInTES                 = the_rootintes      ,
             )
from Configurables import DecayTreeTuple             
from DecayTreeTuple.Configuration import *
from b2oc_aman_b2ddhDalitz.selectB2DDh import configure_b2oc_aman_ddhDalitz_selection
dv.appendToMainSequence( configure_b2oc_aman_ddhDalitz_selection( dataYear = the_year , locationRoot = the_locationRoot ) )

db = CondDB  ( LatestGlobalTagByDataType = the_year )

if '__main__' == __name__ :

  print ' The year : ', the_year

# ================================
# Read only fired events for speed
# ================================

from Configurables import LoKi__HDRFilter as StripFilter
from PhysConf.Filters import LoKi_Filters
fltrs = LoKi_Filters (
    STRIP_Code = """
    HLT_PASS('StrippingB02D0DKBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB02D0DPiBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB2D0D0KBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB2D0D0KD02HHD02HHBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB2DDKBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB2D0DKSLLBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB2D0DKSDDBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB02D0D0KSD02HHD02HHLLBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB02D0D0KSD02HHD02HHDDBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB2DDPiBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB02DDKSDDBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB02DDKSLLBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB02D0DKD02K3PiBeauty2CharmLineDscision') | \
    HLT_PASS('StrippingB02D0DPiD02K3PiBeauty2CharmLineDecision') """
    )

# ================================
# Configure DaVinci
# ================================

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

from Configurables import DaVinci

davinci = DaVinci (
    EventPreFilters = fltrs.filters ('Filters') ,
    EvtMax          =     -1    ,
    PrintFreq       =   1000    ,
    Lumi            =   True    ,
    TupleFile       = "B2OC_AMAN_B2DDHDALITZSEL_2016_CE.ROOT"
    )

