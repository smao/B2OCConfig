# ================================
# Configure DaVinci
# ================================

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

from Configurables import DaVinci

davinci = DaVinci (
    EvtMax          =     -1    ,
    PrintFreq       =   1000    ,
    Lumi            =   False   ,
    TupleFile       = "B2OC_AMAN_B2DDHDALITZSEL.ROOT"
    )

